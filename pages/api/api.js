const API_URL = process.env.WORDPRESS_API_URL !== 'production';
// http://localhost/siteblindado/labs-rest
export const server = API_URL ? 'https://labs.pixelcake.com.br/graphql' : 'https://labs.pixelcake.com.br/graphql';

async function fetchAPI(query, { variables } = {}) {
  const headers = { 'Content-Type': 'application/json' }

  if (process.env.WORDPRESS_AUTH_REFRESH_TOKEN) {
    headers[
      'Authorization'
    ] = `Bearer ${process.env.WORDPRESS_AUTH_REFRESH_TOKEN}`
  }

  const res = await fetch(server, {
    method: 'POST',
    headers,
    body: JSON.stringify({
      query,
      variables,
    }),
  })

  const json = await res.json()
  if (json.errors) {
    console.error(json.errors)
    throw new Error('Failed to fetch API')
  }
  return json.data
}

export async function getPosts(search, offset){
  const data = await fetchAPI(
      `query AllPosts($offset: Int = ${offset}) {
        posts(where: {offsetPagination: {offset: $offset, size: 10}, search: "${search}"}) {
          edges {
            node {
              id
              title
              categories(first: 1) {
                nodes {
                  name
                }
              }
              commentCount
              featuredImage {
                node {
                  sourceUrl
                }
              }
              date
              author {
                node {
                  name
                }
              }
              excerpt(format: RENDERED)
            }
          }
          pageInfo {
            offsetPagination {
              hasPrevious
              hasMore
              total
            }
          }
        }
      }
    `);
  return data?.posts;
}

export async function getSinglePost(id){
    const data = await fetchAPI(`
    query getSinglePost($id: ID = "${id}"){
        post(id:$id){           
          id
          title
          content(format: RENDERED)
        }
      }
    `);

    return data?.post;
}

export async function loadMore(offset) {
  const data = await fetchAPI(
    `query loadMore($offset: Int = ${offset}) {
      posts(where: {offsetPagination: {offset: $offset, size: 10}}) {
        edges {
          node {
            id
            title
            categories(first: 1) {
              nodes {
                name
              }
            }
            commentCount
            featuredImage {
              node {
                sourceUrl
              }
            }
            author {
              node {
                name
              }
            }
            excerpt(format: RENDERED)
          }
        }
        pageInfo {
          offsetPagination {
            hasPrevious
            hasMore
            total
          }
        }
      }
    }
  `);
return data;
}