import Head from "next/head";
import { getSinglePost } from '../api/api.js';

function Post({post}) {
    return (
        <div>
            <Head>
                <title>{post.title}</title>
            </Head>

            <main>
                <div dangerouslySetInnerHTML={{__html:post.content}} />
            </main>
        </div>
    )
}

export default Post;

export const getServerSideProps = async (ctx) => {
    let {id} = ctx.params;
    let post = await getSinglePost(id);
    
    return {
        props:{
            post
        }
    }
}