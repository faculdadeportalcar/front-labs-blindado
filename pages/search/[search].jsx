import Head from 'next/head';
import Header from '../../components/Header/index.jsx';
import { getPosts } from '../api/api.js';
import SearchForm from '../../components/SearchForm/index.jsx';
import CardPost from '../../components/CardPost/index.jsx';

export default function Search({WpPosts}) {
    return (
        <>
            <Head>
                <title>Labs Site Blindado</title>
            </Head>
            
            <Header />
            <main style={{"margin-top":"100px"}}>
                <div className="container">
                    <SearchForm />
                    <div className="posts" style={{"max-width": "700px"}}>
                        {
                            WpPosts.map((post, index) => (
                                <CardPost 
                                key={index}
                                image={post.node.featuredImage}
                                title={post.node.title}
                                author={post.node.author.node.name}
                                excerpt={post.node.excerpt}
                                category={post.node.categories.nodes[0].name}
                                id={post.node.id}
                                />
                            ))
                        }
                    </div>
                </div>
            </main>
            
        </>
    )
}

export const getServerSideProps = async (ctx) => {
    let {search} = ctx.params;
    let posts = await getPosts(search, 0);

    return {
        props: {
            WpPosts: posts.edges
        }
    }
}