import Head from 'next/head';
import { useState, useEffect } from 'react';
import Header from '../components/Header/index.jsx';
import CardPost from '../components/CardPost/index.jsx';
import SearchForm from '../components/SearchForm/index.jsx';
import { getPosts, loadMore } from './api/api.js';


export default function Home({ WpPosts }) {
  const [posts, setPosts] = useState(WpPosts);
  const [offset, setOffset] = useState(0);
  const [hasMore , setHasMore] = useState(true);

  useEffect(() => {
    setPosts(posts);
  }, [posts])
  
  const handlerLoadMore = async () => {
    const offsetState = offset;

    if(hasMore) {
      offsetState += 10;
      setOffset(offsetState);

      const LoadedPosts = await getPosts("", offsetState);
      const LoadedHasMore = LoadedPosts.pageInfo.offsetPagination.hasMore;
      setHasMore(LoadedHasMore);

      const newPosts = posts.concat(LoadedPosts.edges);
      setPosts(newPosts);

      console.log(hasMore)
    }
  }
  

  return (
    <div>
      <Head>
        <title>Labs Site Blindado</title>
      </Head>

      <Header/>
      
      <main style={{"margin-top":"100px"}}>
        <div className="container">
          <SearchForm />
          <div className="posts" style={{"max-width": "700px"}}>

            {
              posts.map((post, index) => (
                <CardPost 
                  key={index}
                  image={post.node.featuredImage}
                  title={post.node.title}
                  author={post.node.author.node.name}
                  excerpt={post.node.excerpt}
                  category={post.node.categories.nodes[0].name}
                  id={post.node.id}
                />
              ))
            }
            <button onClick={handlerLoadMore}>Ver mais</button>
            {/* <button onClick={async () => {
              const newPosts = await loadMore(10);
              const LoadedPosts = newPosts.concat(posts)
              setPosts(LoadedPosts)
            }}>Ver mais</button> */}
          </div>
        </div>
      </main>
    </div>
  )
}

export const getServerSideProps = async () => {
  let posts = await getPosts("", 0);

  return {
    props: {
      WpPosts: posts.edges
    }
  }
}