import Image from 'next/image'
import Link from 'next/link';
import styles from './styles.module.scss';
import { BiCategoryAlt, BiGhost } from 'react-icons/bi';

export default function CardPost({image, title, author, excerpt, id, category}) {
    return (
        <Link href={`/post/${id}`}>
            <a className={styles.post}>
                <div className={styles.content}>

                    <div className={styles.category}>
                        <span>
                            <BiGhost/> {author}
                        </span>
                        <span>
                            <BiCategoryAlt/> {category}
                        </span>
                    </div>
                    
                    <h2 className={styles.title}>
                        {title}
                    </h2>
                    <div>
                    
                    </div>
                    <div dangerouslySetInnerHTML={{ __html: excerpt }}></div>
                    
                </div>
                <div className={styles.image}>
                    {
                        image?
                        <Image src={image.node.sourceUrl} 
                        alt="Picture of the author"
                        width="350px"
                        height="300px"/>
                        :
                        <Image src="/sem-imagem.png"
                        alt="Picture of the author"
                        width="350px"
                        height="300px"/>
                    }
                </div>
            </a>
        </Link>
    )
}