import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import styles from './styles.module.scss';

export default function SearchForm() {
    const [searchFilter, setSearchFilter] = useState("");

    const router = useRouter();
    const ButtonSearchHandler = async (event) => {
        event.preventDefault();
        router.push(`/search/${searchFilter}`);
    }

    return (
        <div className="search">
            <form onSubmit={ButtonSearchHandler}>
              <input type="text" onChange={(e) => setSearchFilter(e.target.value)} />
              <button>buscar</button>
            </form>
        </div>
    )
}