import Link from 'next/link';
import Image from 'next/image'
import styles from './styles.module.scss';

export default function Header() {
    return (
        <header className={styles.header}>
            <div className="container">
                <div className={styles.headerContainer}>
                    <Link href="/">
                        <Image src="/labs-site-blindado.png" alt="Picture of the author"
                        width="100"
                        height="59"/>
                    </Link>
                        
                    <div className={styles.menu}>
                        <Link href="/">
                            <a className={styles.menuItem}>
                                Home
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </header>
    )
}