/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['labs.pixelcake.com.br']
  },
}

module.exports = nextConfig
